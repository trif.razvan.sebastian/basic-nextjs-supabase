import { supabase } from "@/supabaseClient";

const editUser = async (e, formData, id) => {
  e.preventDefault();
  const {
    email,
    isAdmin: admin_role,
    username,
    fullName: full_name,
  } = formData;

  try {
    const { error } = await supabase
      .from("users")
      .update({
        email,
        full_name,
        username,
        admin_role,
      })
      .eq("id", id);

    if (error) {console.log(error)}
    else {
      window.location.reload()
    }
  } catch (error) {
    console.log(error);
  }
};

export default editUser;
