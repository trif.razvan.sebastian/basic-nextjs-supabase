import { supabase } from "@/supabaseClient";

const deleteUser = async (id) => {
  try {
    const { error } = await supabase.from("users").delete().eq("id", id);

    if (error) {
      console.log(error);
    } else {
      window.location.reload();
    }
  } catch (error) {
    console.log(error, "can't delete user");
  }
};

export default deleteUser;
