import { supabase } from "@/supabaseClient";
import { v4 as uuidv4 } from "uuid";

const addUsersToServer = async (e, formData) => {
  e.preventDefault();
  const {
    email,
    isAdmin: admin_role,
    username,
    fullName: full_name,
  } = formData;
  console.log({ ...formData, id: uuidv4()});
  try {
    const { error } = await supabase
    .from("users")
    .insert({
      id: uuidv4(),
      email,
      admin_role,
      username,
      full_name,
      created_at: new Date()
    })
    .single()

    if (error) {console.log(error)}
    else {
        window.location.reload();
      console.log("success");
    }

  } catch (error) {
    console.log(error.message, "can't add user");
  }
};

export default addUsersToServer;
