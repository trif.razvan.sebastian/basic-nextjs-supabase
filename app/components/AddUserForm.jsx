"use client";

import { useState } from "react";
import addUsersToServer from "../utils/addUsersToServer";

export default function AddUserForm() {
  const [formData, setFormData] = useState({
    email: "",
    fullName: "",
    username: "",
    isAdmin: false,
  });

  const changeFormValue = (e) => {
    const { name, value } = e.target;

    setFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const check = () => {
    setFormData((prev) => {
      return {
        ...prev,
        isAdmin: !prev.isAdmin,
      };
    });
  };

  return (
    <div>
      <form
        onSubmit={(e) => addUsersToServer(e, formData)}
        action="/"
        method="post"
      >
        <label htmlFor="add-email">Email</label>
        <input
          id="add-email"
          onChange={changeFormValue}
          name="email"
          type="email"
        />
        <br />
        <label htmlFor="add-name">Full name</label>
        <input
          id="add-name"
          onChange={changeFormValue}
          name="fullName"
          type="text"
        />
        <br />
        <label htmlFor="add-username">Username</label>
        <input
          id="add-username"
          onChange={changeFormValue}
          name="username"
          type="text"
        />
        <br />
        <label htmlFor="add-check">Is admin?</label>
        <input id="add-check" onChange={check} type="checkbox" name="isAdmin" />
        <br />
        <button type="submit">Add user</button>
      </form>
    </div>
  );
}
