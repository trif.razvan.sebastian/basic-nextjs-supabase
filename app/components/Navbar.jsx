import Link from "next/link";
import Flex from "./Flex";

export default function Navbar() {
  return (
    <Flex justify="center" align="center" gap="2rem">
        <Link href="/">home</Link>
        <br />
        <Link href="/account">account</Link>
    </Flex>
  )
}