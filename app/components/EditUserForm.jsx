"use client";

import { useState } from "react";
import editUser from "../utils/editUser";

export default function EditUserForm({ user }) {
  const {
    full_name: fullName,
    email,
    username,
    id,
    admin_role: isAdmin,
  } = user;

  const [formData, setFormData] = useState({
    email: email || "",
    fullName: fullName || "",
    username: username || "",
    isAdmin: isAdmin || false,
    profileImage: null,
  });

  const changeFormValue = (e) => {
    const { name, value } = e.target;

    setFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const check = () => {
    setFormData((prev) => {
      return {
        ...prev,
        isAdmin: !prev.isAdmin,
      };
    });
  };

  return (
    <div>
      <form
        onSubmit={(e) => editUser(e, formData, id)}
        action="/"
        method="post"
      >
        <input
          value={formData.email}
          onChange={changeFormValue}
          name="email"
          type="email"
        />
        <input
          value={formData.fullName}
          onChange={changeFormValue}
          name="fullName"
          type="text"
        />
        <input
          value={formData.username}
          onChange={changeFormValue}
          name="username"
          type="text"
        />
        <input
          defaultChecked={isAdmin}
          onChange={check}
          type="checkbox"
          name="isAdmin"
        />
        <button type="submit">Apply changes</button>
      </form>
    </div>
  );
}
