import React from "react";

export default function Flex({ children, justify, align, gap }) {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: justify ? justify : "left",
        alignItems: align ? align : "flex-start",
        gap: gap ? gap : "0px",
      }}
    >
      {children}
    </div>
  );
}
