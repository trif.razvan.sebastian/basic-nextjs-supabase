"use client";

import { useState, useEffect } from "react";
import { supabase } from "@/supabaseClient";

const useGetUsers = () => {
  const [usersArr, setUsersArr] = useState(null);

  const getUsers = async () => {
    try {
      const { data, error } = await supabase
        .from("users")
        .select("*")
        .limit(10);
      if (error) throw error;
      if (data != null) {
        setUsersArr(data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getUsers();
    console.log("got the users")
  }, []);

return { getUsers, usersArr }

};

export default useGetUsers;