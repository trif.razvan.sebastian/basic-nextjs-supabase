"use client";

import { useState } from "react";
import AddUserForm from "./components/AddUserForm";
import useGetUsers from "./hooks/useGetUsers";
import deleteUser from "./utils/deleteUser";
import EditUserForm from "./components/EditUserForm";

export default function Home() {
  const { usersArr } = useGetUsers();
  const [editMode, setEditMode] = useState(false);

  return (
    <div>
      <p>Home page</p>
      <AddUserForm />
      <br />
      <div>
        {usersArr?.map((user, i) => {
          const { full_name, email, username, id, admin_role } = user;
          return editMode ? (
            <div
              style={{
                border: "1px solid black",
                width: "15em",
                marginBottom: "1rem",
              }}
              key={i}
            >
              <EditUserForm user={user} />
              <button onClick={() => setEditMode((prev) => !prev)}>Edit</button>
            </div>
          ) : (
            <div
              style={{
                border: "1px solid black",
                width: "15em",
                marginBottom: "1rem",
              }}
              key={i}
            >
              <h3>{username}</h3>
              {admin_role ? <h5>Is admin</h5> : <h5>Not admin</h5>}
              <p>{full_name}</p>
              <p>{email}</p>
              <button onClick={() => setEditMode((prev) => !prev)}>Edit</button>
              <button onClick={() => deleteUser(id)}>Delete</button>
            </div>
          );
        })}
      </div>
    </div>
  );
}
